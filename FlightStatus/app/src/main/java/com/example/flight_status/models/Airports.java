package com.example.flight_status.models;

public class Airports {


    private String name;
    private String city;
    private String countryName;
    private String regionName;
    private String localTime;
    private String classification;

    public Airports(String name, String city, String countryName, String regionName, String localTime, String classification) {
        this.name = name;
        this.city = city;
        this.countryName = countryName;
        this.regionName = regionName;
        this.localTime = localTime;
        this.classification = classification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }
}
