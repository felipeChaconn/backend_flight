package com.example.flight_status;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.flight_status.utilities.Utilities;

public class ConnectionSQLiteHelper extends SQLiteOpenHelper {

    public ConnectionSQLiteHelper( Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //This section call the constants to execute the SQLite script on they, creating the database tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilities.Users_table);
    }

    //This section check previous if the database tables already exist to delete and create they again every time that the app will be installed
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        onCreate(db);
    }



    //Methods to open and close the BD connection

    //Open connection
    public void openConnection(){
        this.getWritableDatabase();
    }

    //Close connection
    public void  closeConnection(){
        this.close();
    }


    //Insert users into database
    public void registryUsers(String Uname, String Ulastname, String Unickname, String Uemail, String Upassword){
        ContentValues user = new ContentValues();
        user.put("name",Uname);
        user.put("lastname",Ulastname);
        user.put("nickname",Unickname);
        user.put("email",Uemail);
        user.put("password",Upassword);
        this.getWritableDatabase().insert("users",null,user);
    }

    //Login
    public Cursor Login(String user, String password) throws SQLException {
           Cursor mcursor=null;
           mcursor=this.getReadableDatabase().query("users",
                   new String[]{"name","lastname","nickname","email","password"},
                   "email like '"+user+"' OR "+"nickname like '"+user+"' AND password like '"+password+"'",
                   null,null,null,null);
           return mcursor;
    }
}
