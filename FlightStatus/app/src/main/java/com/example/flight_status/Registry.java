package com.example.flight_status;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registry extends AppCompatActivity {

    Button complete, cancel;
    EditText nameU, lastnameU, nicknameU,emailU,passwordU;

    ConnectionSQLiteHelper helper = new ConnectionSQLiteHelper(this,"FlightstatsBD",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);

        complete=(Button)findViewById(R.id.registry_completeBTN);
        nameU=(EditText)findViewById(R.id.nameTXT);
        lastnameU=(EditText)findViewById(R.id.lastnameTXT);
        nicknameU=(EditText)findViewById(R.id.nicknameTXT);
        emailU=(EditText)findViewById(R.id.emailTXT);
        passwordU=(EditText)findViewById(R.id.passwordTXT);
        cancel=(Button)findViewById(R.id.registry_cancelBTN2);

        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.openConnection();
                helper.registryUsers(
                        String.valueOf(nameU.getText()),
                        String.valueOf(lastnameU.getText()),
                        String.valueOf(nicknameU.getText()),
                        String.valueOf(emailU.getText()),
                        String.valueOf(passwordU.getText()));
                helper.closeConnection();
                Toast.makeText(getApplicationContext(),"User registered",Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new  View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }
}
