package com.example.flight_status;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Airlines extends AppCompatActivity {
    private RequestQueue mQueue;
    Button airlinebtn;
    TextView airlinetv;
    EditText airlinetxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airlines);
        mQueue = Volley.newRequestQueue(this);

        airlinetxt = (EditText)findViewById(R.id.AirlinesTXT);
        airlinebtn = (Button)findViewById(R.id.airlinesBTN);
        airlinetv = (TextView)findViewById(R.id.airlinesTV);

        airlinebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airlinetv.setText("");
                LoadAirlinesbyIATACode(airlinetxt.getText().toString());
            }
        });
    }


    private  void LoadAirlinesbyIATACode(String code) {
        String flightURL ="https://api.flightstats.com/flex/airlines/rest/v1/json/fs/"+code+"?appId=f19a01f8&appKey=e1d25151d55570fe8656cbbc90596e9d";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, flightURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject Jsonarray =response.getJSONObject("airline");
                            for (int i=0; i < Jsonarray.length(); i++){
                                    String fs = Jsonarray.getString("fs");
                                    String iata = Jsonarray.getString("iata");
                                    String icao = Jsonarray.getString("icao");
                                    String name = Jsonarray.getString("name");
                                    String phone = Jsonarray.getString("phoneNumber");
                                    String status = Jsonarray.getString("active");
                                    airlinetv.setText("FS Code: "+fs+"\n"+"IATA Code: "+iata+"\n"+"ICAO: "+icao+"\n"+"Name: "+name+"\n"+"Phone Number: "+phone+
                                            "\n"+"Status: "+status);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}
