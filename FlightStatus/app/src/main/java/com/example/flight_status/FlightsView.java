package com.example.flight_status;

import android.app.NotificationManager;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.flight_status.models.Airports;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FlightsView extends AppCompatActivity {
    RequestQueue mQueue;
    Button searchbtn;
    EditText searchtxt;
    TextView result;
    NotificationCompat.Builder notifications;
    private static final int uniqueIndex = 007;



    ArrayList<Airports> airportsList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flights_view);
        mQueue = Volley.newRequestQueue(this);
        notifications = new NotificationCompat.Builder(this);
        notifications.setAutoCancel(true);

        searchtxt = (EditText)findViewById(R.id.searchAirportsTXT);
        searchbtn = (Button)findViewById(R.id.searchAirportsBTN);
        result = (TextView) findViewById(R.id.searchboard);
        result.setMovementMethod(new ScrollingMovementMethod());
        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airportsList.clear();
                result.setText("");
                SearchAirportsByCountry( searchtxt.getText().toString());
            }
        });
    }


    private void createNotifications(Integer arraySize){
        notifications.setSmallIcon(R.mipmap.ic_launcher_round);
        notifications.setTicker("New notification");
        notifications.setWhen(System.currentTimeMillis());
        notifications.setContentTitle("Your search about airports is ready");
        notifications.setContentText(arraySize+" is the total of airports in the country selected");

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueIndex,notifications.build());
    }


    private void SearchAirportsByCountry(String countryCode) {

        String airportsURL ="https://api.flightstats.com/flex/airports/rest/v1/json/countryCode/"+countryCode+"?appId=f19a01f8&appKey=e1d25151d55570fe8656cbbc90596e9d";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, airportsURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray Jsonarray = response.getJSONArray("airports");
                            for (int i = 0; i < Jsonarray.length(); i++) {
                                JSONObject flight = Jsonarray.getJSONObject(i);
                                String name = flight.getString("name");
                                String city = flight.getString("city");
                                String countryName = flight.getString("countryName");
                                String regionName = flight.getString("regionName");
                                String localTime = flight.getString("localTime");
                                String classification = flight.getString("classification");
                                Airports newAirport = new Airports(name, city, countryName, regionName, localTime, classification);
                                airportsList.add(newAirport);
                                String airport;
                                for (Airports air : airportsList) {
                                    airport = (
                                            "Name: " + air.getName() + "\n" +
                                                    "City: " + air.getCity() + "\n" +
                                                    "Country: " + air.getCountryName() + "\n" +
                                                    "Region: " + air.getRegionName() + "\n" +
                                                    "Local Time: " + air.getLocalTime() + "\n" +
                                                    "Classification: " + air.getClassification() + "\n" + "\n");
                                    result.append(airport);
                                    airportsList.remove(air);
                                }

                            }
                            final Integer arraySize = Jsonarray.length();
                            Handler handler  = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    createNotifications(arraySize);
                                }
                            },2000);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                 }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
        }
    });
        mQueue.add(request);
}
}