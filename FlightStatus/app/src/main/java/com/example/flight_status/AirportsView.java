package com.example.flight_status;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.flight_status.models.Airports;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class AirportsView extends AppCompatActivity {

    NotificationCompat.Builder airportNotification;
    private static final int uniqueAirportsID=47;
    final ArrayList<String> latitudeArray = new ArrayList<>();
    final ArrayList<String> longitudeArray = new ArrayList<>();

    RequestQueue mQueue;
    EditText airlineCode,flightNumber,year,month,day;
    Button load;
    TextView flightIdCard, airlineCodeCard, flightNumberCard, airportDepCard, airportArrCard, flightStatCard;
    GridLayout result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airports_view);
        //API request
        mQueue = Volley.newRequestQueue(this);

        //Notifications
        airportNotification = new NotificationCompat.Builder(this);
        airportNotification.setAutoCancel(true);

        //Visual components
        airlineCode =(EditText)findViewById(R.id.airline);
        flightNumber =(EditText)findViewById(R.id.flightNumber);
        year =(EditText)findViewById(R.id.year);
        month =(EditText)findViewById(R.id.month);
        day =(EditText)findViewById(R.id.day);
        flightIdCard=(TextView) findViewById(R.id.flightIdCard);
        airlineCodeCard=(TextView) findViewById(R.id.airlineCodeCard);
        flightNumberCard=(TextView) findViewById(R.id.flightNumberCard);
        airportDepCard=(TextView) findViewById(R.id.airportDepCard);
        airportArrCard=(TextView) findViewById(R.id.airportArrCard);
        flightStatCard=(TextView) findViewById(R.id.flightStatCard);
        result=(GridLayout)findViewById(R.id.searchboard);
        load =(Button)findViewById(R.id.search);
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                result.setVisibility(View.VISIBLE);
                latitudeArray.clear();
                longitudeArray.clear();
                FlightArrivingOnDate(airlineCode.getText().toString(),
                        flightNumber.getText().toString(),year.getText().toString(),month.getText().toString(),
                        day.getText().toString());
            }
        });
    }
    private void FlightArrivingOnDate(String carrierFsCode,String flightNumber,String year, String month, String day) {
        String flightURL ="https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/"+carrierFsCode+"/"+flightNumber+"/arr/"+year+"/"+month+"/"+day+"?appId=f19a01f8&appKey=e1d25151d55570fe8656cbbc90596e9d&utc=false";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, flightURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray Jsonarray =response.getJSONArray("flightStatuses");
                            for (int i=0; i < Jsonarray.length(); i++){
                                JSONObject flight = Jsonarray.getJSONObject(i);
                                String flightId = flight.getString("flightId");
                                String carrierFsCode = flight.getString("carrierFsCode");
                                String flightNumber = flight.getString("flightNumber");
                                String departureAirportFsCode = flight.getString("departureAirportFsCode");
                                String arrivalAirportFsCode = flight.getString("arrivalAirportFsCode");
                                String status = flight.getString("status");
                                flightIdCard.setText("Flight id: " + flightId);
                                airlineCodeCard.setText("Airline code: " + carrierFsCode);
                                flightNumberCard.setText("Flight number: " + flightNumber);
                                airportDepCard.setText("Dep airport: " + departureAirportFsCode);
                                airportArrCard.setText("Arr airport: " + arrivalAirportFsCode);
                                flightStatCard.setText("Flight stat: " + status);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
        FindAirportsLocations(flightURL);
    }

    private void FindAirportsLocations(String flightURL) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, flightURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            final String la1;
                            final String la2;
                            final String lon1;
                            final String lon2;
                            JSONObject appendix =response.getJSONObject("appendix");
                            for (int i=0; i < appendix.length(); i++){
                                JSONArray indexAirport = appendix.getJSONArray("airports");
                                for (int j=0;j < indexAirport.length(); j++){
                                    JSONObject airports =indexAirport.getJSONObject(j);
                                    String latitude = airports.getString("latitude");
                                    String longitude = airports.getString("longitude");
                                    latitudeArray.add(latitude);
                                    longitudeArray.add(longitude);
                                }
                            }
                            //Airport 1
                            if (!latitudeArray.isEmpty() && !longitudeArray.isEmpty()) {
                                la1 = latitudeArray.get(0);
                                lon1 = longitudeArray.get(0);
                                //Airport 2
                                la2 = latitudeArray.get(1);
                                lon2 = longitudeArray.get(1);

                                Handler handler  = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        AirportsNotification(la1, la2, lon1, lon2);
                                    }
                                },5000);
                            }
                            else {
                                Toast.makeText(getApplicationContext(),"This fligh doesn't exist... try with other flight number. ",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    public void AirportsNotification(String la1,String la2, String lon1 ,String lon2){
        airportNotification.setSmallIcon(R.mipmap.ic_launcher_round);
        airportNotification.setTicker("You have new notification");
        airportNotification.setWhen(System.currentTimeMillis());
        airportNotification.setContentTitle("Your search about flight");
        airportNotification.setContentText("Would you like see the location?");

        Intent intent = new Intent(new Intent(AirportsView.this,FlightsView.class));
        PendingIntent pendingIntent = PendingIntent.getActivity(AirportsView.this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        airportNotification.setContentIntent(pendingIntent);

        NotificationManager mn = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mn.notify(uniqueAirportsID,airportNotification.build());
    }


}
