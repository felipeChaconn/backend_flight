package com.example.flight_status;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.flight_status.Airports.GetallAirports;
//import com.example.flight_status.Airports.GetallAirports.RetroAirports;
//import com.example.flight_status.Airports.AirportAdapter;
//import com.example.flight_status.Airports.RetrofitClientAirport;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvRegistry;
        Button btnLogin;

        final ConnectionSQLiteHelper helper = new ConnectionSQLiteHelper(this,"FlightstatsBD",null,1);

        tvRegistry=(TextView)findViewById(R.id.tvRegistry);
        tvRegistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Registry.class);
                startActivity(i);
            }
        });

        btnLogin=(Button) findViewById(R.id.Login_User_BTN);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtUserName = (EditText)findViewById(R.id.usuTXT);
                EditText txtUserPass = (EditText)findViewById(R.id.passTXT);
                try {
                    Cursor cursor = helper.Login(txtUserName.getText().toString(),txtUserPass.getText().toString());
                    if (cursor.getCount()>0){
                        Intent i = new Intent(getApplicationContext(),PrincipalView.class);
                        startActivity(i);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"User or pass are incorrect",Toast.LENGTH_LONG).show();
                    }
                    txtUserName.setText("");
                    txtUserPass.setText("");
                    txtUserName.findFocus();

                }
                catch (SQLException e){
                    e.printStackTrace();
                }

            }
        });

    }

}


