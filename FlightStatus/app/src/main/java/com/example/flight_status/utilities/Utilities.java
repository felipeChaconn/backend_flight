package com.example.flight_status.utilities;

public class Utilities {

    //This class is to have a location to place all the constants and call they from every part of the project easier.

    //This constants are executes every time that the app is installed on the phone, with the objective to create the Database tables
       public static final String TABLE_USER = "users";
    public static final String CAMPO_ID = "_ID";
    public static final String CAMPO_NAME = "name";
    public static final String CAMPO_LASTNAME = "lastname";
    public static final String CAMPO_EMAIL = "email";
    public static final String CAMPO_NICKNAME = "nickname";
    public static final String CAMPO_PASS = "password";


    public static final String Users_table="CREATE TABLE "+TABLE_USER+ "("+CAMPO_ID+" INTEGER PRIMARY KEY,"+CAMPO_NAME+" TEXT,"+ CAMPO_LASTNAME+" TEXT,"+
            CAMPO_EMAIL+" TEXT,"+CAMPO_NICKNAME+" Text,"+CAMPO_PASS+" TEXT)";

    public static final String Airport_table="CREATE TABLE airports (fs TEXT,iata TEXT,icao TEXT,faa TEXT, name TEXT, city TEXT, stateCode TEXT, countryCode TEXT," +
            " countryName TEXT, regionName TEXT,timeZoneRegionName TEXT,weatherZone TEXT,localTime TEXT,latitude TEXT, longitude TEXT," +
            " weatherUrl TEXT, active NUMERIC)";
    public static final String Airline_table="CREATE TABLE airlines(_ID INTEGER PRIMARY KEY, fs TEXT, iata TEXT,name TEXT, active NUMERIC)";


}
