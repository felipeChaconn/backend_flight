package com.example.flight_status;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.flight_status.models.Airports;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PrincipalView extends AppCompatActivity {

    private RequestQueue mQueue;
    final ConnectionSQLiteHelper helper = new ConnectionSQLiteHelper(this,"FlightstatsBD",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_view);

        CardView airportsCard;
        CardView flightsCard;
        CardView airlinesCard;

        airportsCard=(CardView)findViewById(R.id.card_airports);
        airportsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AirportsView.class);
                startActivity(i);
            }
        });
        
        flightsCard=(CardView)findViewById(R.id.card_flights);
        flightsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FlightsView.class);
                startActivity(i);
            }
        });

        airlinesCard=(CardView)findViewById(R.id.card_schedules);
        airlinesCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Airlines.class);
                startActivity(i);
            }
        });

        mQueue = Volley.newRequestQueue(this);
    }








}
